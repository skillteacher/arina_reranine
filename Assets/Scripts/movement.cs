using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private KeyCode jumpButton = KeyCode.Space;    
    [SerializeField] private Collider2D feetcollider;
    [SerializeField] private string groundLayer = "Ground";
    [SerializeField] private float jumpForce = 20f;

    private Rigidbody2D playerRigidbody;
    private Animator playerAnimator;
    private SpriteRenderer playerSR;
    private bool isGrounded = true;

    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        playerAnimator = GetComponent<Animator>();
        playerSR = GetComponent<SpriteRenderer>();
    }


    private void Update()
    {
        float playerInput = Input.GetAxis("Horizontal");
        Move(playerInput);
        Flipe(playerInput);
        //isGrounded = feetcollider.IsTouchingLayers(LayerMask.GetMask(groundLayer));
        if (Input.GetKeyDown(jumpButton) && isGrounded)
        {
            Jump();
        }
    }


    private void Move(float direction)
    {
        playerAnimator.SetBool("Run", direction != 0);

        playerRigidbody.velocity = new Vector2(direction * speed, playerRigidbody.velocity.y);
    }

    private void Jump()
    {
        Vector2 jumpVector = new Vector2(playerRigidbody.velocity.x, jumpForce);
        playerRigidbody.velocity += jumpVector;
    }

    private void Flipe(float direction)
    {
        if(direction > 0)
        {
            playerSR.flipX = false;
        }
        if(direction < 0)
        {
            playerSR.flipX = true;
        }
            
        
    }



}



      